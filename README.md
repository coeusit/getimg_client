# GetimgClient [![Gem Version](https://badge.fury.io/rb/getimg_client.svg)](https://badge.fury.io/rb/getimg_client)

## Introduction
GetimgClient is a Ruby gem designed to interact with the Getimg.ai API, enabling users to generate, manipulate, and enhance images using various AI models. This gem simplifies the process of making API requests to Getimg.ai, allowing for seamless integration of image generation and manipulation capabilities into your Ruby applications.

## Installation

To add it to a project using bundler, run the following command:

```bash
bundle add getimg_client
```

Alternatively you can add this line to your application's Gemfile:

```ruby
gem 'getimg_client'
```

And then execute:

```bash
bundle install
```

Or install it yourself as:

```bash
gem install getimg_client
```

Optionally, set the API key using the environment variable:

```bash
export GETIMG_API_KEY=your_api_key
```

## Functions

### `set_api_key(api_key)`
Set or override the API key used for authentication with the Getimg.ai API. This key can also be set by providing the **GETIMG_API_KEY** environment variable.

### `retrieve_models`
Fetch and cache the list of available models from the API.

### `models`
Retrieve cached models or fetch them if not already retrieved.

### `generate_image(prompt, model: :essential, **options)`
Generate an image based on a text prompt and specified model. Supports various image manipulation methods such as image-to-image, inpainting, controlnet, face fixing, instruct, and upscaling. Specifying a model is optional; by default, it will invoke the Essential V1 endpoint.

### `get_balance`
Retrieve the current account balance from the Getimg.ai API.

## Request Routing
The `generate_image` method routes requests based on the provided options and the model's supported pipelines, sending it to its respective pipeline whether that's SD1.5, SDXL, Essential, or LCM based:
- **text-to-image**: Default if no images are provided.
- **image-to-image**: Triggered if a base image is provided.
- **inpaint**: Triggered if a mask image is provided.
- **controlnet**: Triggered if controlnet options are provided.
- **face-fix**: Triggered if the model supports face-fix and a base image is provided.
- **upscale**: Triggered if the model supports upscale and a base image is provided.
- **instruct**: Triggered if the model supports instruct and a base image is provided.

## Parameters
Parameters for `generate_image` and other methods are consistent with the official Getimg.ai API documentation. For detailed parameter descriptions, refer to the [official API documentation](https://docs.getimg.ai/reference/introduction).

## Error Reporting
Errors in the `GetimgClient` are handled through exceptions. The `handle_response` method is used to process API responses, raising errors when necessary:

- **ArgumentError**: Raised for invalid arguments or unknown models.
- **StandardError**: Raised for HTTP errors or API response errors.

API errors are captured from the JSON response and included in the raised exception message. This includes both HTTP status errors and specific API error messages. For example:

```ruby
raise StandardError, "Error: #{result['error']['message']} (code: #{result['error']['code']})"
```

If an HTTP error occurs, the error message will include both the HTTP status and the error message from the API response, if available.

## Models
The provided `model` option will determine the model use, and contribute to the client's inference of the desired endpoint. Models can be the string `id` listed online at [the GetImg dashboard](https://dashboard.getimg.ai/models) or retrieved using the `GetimgClient.models` method. Equally, you can use symbols instead. For example, `:realistic_vision_v5_1` will translate to `'realistic-vision-v5-1'`

## Essential
Getimg offers the "essential" checkpoints, which perform more abstract Stable Diffusion operations based on the provided prompt. Note that *:essential* and *:essential_v2* will *not* be listed in the model listing, as these are not in fact actual models, nor valid "model" values at the API's end.
In order to route a request to Essential or Essential V2 however, you can provide *:essential* or *:essential_v2* as a model argument in *#generate_image*.

## Latent Consistency Models (LCM)
Latent Consistency Models (LCM) are optimized for faster image generation and lower costs by avoiding the repetitive steps of traditional diffusion methods. They work in a lower-dimensional space, resulting in quicker outputs but with slightly less detail compared to standard models.

## Base Image Options
The `base_image` option can be provided, which will automatically set the "image" property for image-to-image, controlnet, face-fix, upscale, inpaint and instruct calls, and can be either a file path or a base64 encoded string of the file contents.

## Results
If all went well and no errors were reported, the response will be equal to the API response received, and thus align with the official documentation. Typically, this will be a hash containing an `image` property with the Base64 encoded image contents, a `seed` property with the generation's seed value, and a `cost` property with the usage cost for the generation.

## Code Samples

### Set API Key
```ruby
GetimgClient.set_api_key('your_api_key')
```

### Generate Text-to-Image using Essential V1 with default options
```ruby
result = GetimgClient.generate_image("A city skyline at night")
puts result["image"]
```

### Generate Text-to-Image using Stable Diffusion 1.5
```ruby
result = GetimgClient.generate_image("A scenic landscape", model: :stable_diffusion_v1_5, width: 512, height: 512)
puts result["image"]
```

### Generate Image-to-Image
```ruby
base_image = "path/to/base_image.jpg"
result = GetimgClient.generate_image("Enhance the scene", model: :stable_diffusion_v1_5, base_image:, strength: 0.7)
puts result["image"]
```

### Generate an image using Controlnet
```ruby
base_image = "path/to/base_image.jpg"
result = GetimgClient.generate_image("Enhance the scene", model: :stable_diffusion_v1_5, base_image:, strength: 0.7, controlnet: 'canny-1.1')
puts result["image"]
```

### Generate Inpainting
```ruby
base_image = Base64.strict_encode64(File.read("path/to/base_image.jpg"))
mask_image = Base64.strict_encode64(File.read("path/to/mask_image.jpg"))
result = GetimgClient.generate_image("Fill the white area with sky", model: :stable_diffusion_v1_5_inpainting, base_image:, mask_image_path: mask_image)
puts result["image"]
```

### Face Fix
```ruby
base_image = Base64.strict_encode64(File.read("path/to/base_image.jpg"))
result = GetimgClient.generate_image("Fix faces", model: :gfpgan_v1_3, base_image:)
puts result["image"]
```

### Upscale
```ruby
base_image = Base64.strict_encode64(File.read("path/to/base_image.jpg"))
result = GetimgClient.generate_image("Upscale image", model: :real_esrgan_4x, base_image:, scale: 4)
puts result["image"]
```

### Instruct
```ruby
base_image = Base64.strict_encode64(File.read("path/to/base_image.jpg"))
result = GetimgClient.generate_image("Make it night time", model: :instruct_pix2pix, base_image:)
puts result["image"]
```

## Logging
GetimgClient supports logging request details for debugging and development purposes. The logging behavior depends on the environment and configuration:

Rails Development Environment: If the **RAILS_ENV** environment variable is set to "development" and Rails is defined, logs will be sent to the Rails logger.

**GETIMG_LOG** Environment Variable: If the GETIMG_LOG environment variable is set, logs will be printed to the standard output.

#### Log output example
```text
Generating image with the following details:
Prompt: A scenic landscape
Model: stable-diffusion-v1-5
Requested Pipeline: text-to-image
Endpoint URI: https://api.getimg.ai/v1/stable-diffusion/text-to-image
Options: {width: 512, height: 512}
```

## Unit Testing
The gem includes unit tests using Minitest. Integration tests make real API calls and will incur usage fees. Ensure your API key is set in the environment before running tests.

To run the integration tests:

```bash
GETIMG_API_KEY=your_api_key rake integration
```

Integration test images are stored in the tmp directory for review.