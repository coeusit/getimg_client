# 0.1.6
- Hotfix for missing Rails environment variables.

# 0.1.5
- Resolved issue with development logs not including prompt information.
- Adjusted method for logging, supporting both console output and Rails logger.
- Included integration tests for console logging and Rails logger.

# 0.1.4
- Added support for essential and essential-v2 models with custom endpoint routing.
- Adjusted the generate_image method to omit the model parameter for essential and essential-v2.
- Updated integration tests to include essential model testing.

# 0.1.3
- Added source code reference in Gemspec metadata

# 0.1.2
- Touched up documentation to explicitly mention instruct requests, and explain LCM models
- Repackaged gem to include up to date README.md

# 0.1.1
- Adjusted Gemspec to include JSON files
- Revised README to include installation instructions

# 0.1.0
- Initial public release