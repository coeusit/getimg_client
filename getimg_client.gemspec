Gem::Specification.new do |spec|
  spec.name          = "getimg_client"
  spec.version       = "0.1.6"
  spec.authors       = ["Melvin Sommer"]
  spec.email         = ["sommer.melvin@gmail.com"]

  spec.summary       = %q{Client for Getimg API}
  spec.description   = %q{A Ruby client for interfacing with the Getimg API.}
  spec.homepage      = "https://gitlab.com/coeusit/getimg_client"
  spec.license       = "MIT"
  spec.metadata    = { "source_code_uri" => "https://gitlab.com/coeusit/getimg_client" }

  spec.files         = Dir["lib/**/*.rb", "lib/**/*.json", "test/**/*.{rb,jpeg}", "README.md"]
  spec.test_files    = Dir["test/**/*.{rb,jpeg}"]

  spec.required_ruby_version = ">= 3.0"

  spec.add_development_dependency "minitest", "~> 5.11"
end
