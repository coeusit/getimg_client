class GetimgClient
  require 'base64'
  require 'net/http'
  require 'json'
  require 'uri'

  # Load the API endpoints from a JSON file
  API_ENDPOINTS = JSON.parse(File.read(File.join(__dir__, 'endpoints.json'))).freeze

  # Initialize the API key from environment variable
  @api_key = ENV['GETIMG_API_KEY']

  # Set or override the API key
  def self.set_api_key(api_key)
    @api_key = api_key
  end

  # Retrieve models from the API
  def self.retrieve_models
    uri = URI(API_ENDPOINTS['models'])
    request = Net::HTTP::Get.new(uri)
    request['Authorization'] = "Bearer #{@api_key}"
    request['accept'] = 'application/json'

    response = make_get_request(uri, request)
    @models = parse_models_response(response)
  end

  # Get models, retrieving them if not already fetched
  def self.models
    @models ||= retrieve_models
  end

  # Generate an image based on the prompt and model
  def self.generate_image(prompt, model: :essential, **options)
    # Validate the prompt
    raise ArgumentError, 'Prompt is required' unless prompt.is_a?(String) && !prompt.strip.empty?

    # Extract image paths from options
    base_image = options.delete(:base_image)
    mask_image_path = options.delete(:mask_image_path)
    controlnet = options.delete(:controlnet)

    # Convert model to appropriate ID format
    model_id = model.to_s.gsub('_', '-')
    if %w[essential essential-v2].include?(model_id)
      model_info = models[model_id] || { name: model_id, pipelines: ['text-to-image'] }
    else
      model_info = models[model_id] || raise(ArgumentError, "Unknown model: #{model}")
    end

    # Determine the requested pipeline
    requested_pipeline = method_requested(base_image, mask_image_path, controlnet, model_info).to_s.gsub('_', '-')

    # Check if the model supports the requested pipeline
    unless model_info[:pipelines].include?(requested_pipeline)
      raise ArgumentError, "#{model_info[:name]} does not support #{requested_pipeline}"
    end

    # Determine the endpoint key based on the model family and pipeline
    endpoint_key = determine_endpoint_key(model_info, requested_pipeline, model_id)
    uri = URI(API_ENDPOINTS[endpoint_key.to_s])

    # Handle image to image, controlnet, instruct, inpaint, face-fix, and upscale pipelines
    if %w[image-to-image controlnet instruct inpaint face-fix upscale].include?(requested_pipeline)
      image_base64 = validate_file_or_base64(base_image)
      options.merge!(image: image_base64)
      options.merge!(controlnet: controlnet) if requested_pipeline == 'controlnet'
      if requested_pipeline == 'inpaint'
        mask_image_base64 = validate_file_or_base64(mask_image_path)
        options.merge!(mask_image: mask_image_base64)
      end
    end

    # Ensure scale is an integer if present
    options[:scale] = options[:scale].to_i if options[:scale]

    # Log the request details
    log_request_details(prompt, model_id, requested_pipeline, uri, options)

    # Create and send the request
    request = create_request(uri, prompt, model: model_id, **options)
    response = make_post_request(uri, request)
    handle_response(response)
  end

  # Retrieve the current account balance
  def self.get_balance
    uri = URI(API_ENDPOINTS['balance'])
    request = Net::HTTP::Get.new(uri)
    request['Authorization'] = "Bearer #{@api_key}"
    request['accept'] = 'application/json'

    response = make_get_request(uri, request)
    handle_response(response)
  end

  private

  # Log request details
  def self.log_request_details(prompt, model_id, requested_pipeline, uri, options)
    log_message = <<~LOG
      Generating image with the following details:
      Prompt: #{prompt}
      Model: #{model_id}
      Requested Pipeline: #{requested_pipeline}
      Endpoint URI: #{uri}
      Options: #{options.transform_values { |v| v.is_a?(String) && v.length > 50 ? "#{v[0, 50]}..." : v }}
    LOG

    if (ENV['RAILS_ENV'] === 'development' || ENV['RAILS_ENV'].nil?) && defined?(Rails) && Rails.logger
      Rails.logger.info(log_message)
    end
    if ENV['GETIMG_LOG']
      puts log_message
    end
  end

  # Determine the requested method based on provided image paths, controlnet, and model_info
  def self.method_requested(base_image, mask_image_path, controlnet, model_info)
    return :controlnet if controlnet
    return :inpaint if mask_image_path
    return :instruct if model_info[:pipelines].include?('instruct') # Adjusting for instruct model
    return :face_fix if base_image && !mask_image_path && !controlnet && model_info[:pipelines].include?('face-fix')
    return :upscale if base_image && !mask_image_path && !controlnet && model_info[:pipelines].include?('upscale')
    return :image_to_image if base_image
    :text_to_image
  end

  # Determine the appropriate endpoint key based on the model family and pipeline
  def self.determine_endpoint_key(model_info, requested_pipeline, model_id)
    if model_id == 'essential'
      return :essential_text_to_image
    elsif model_id == 'essential-v2'
      return :essentialv2_text_to_image
    elsif model_info[:family] == 'stable-diffusion-xl'
      return :sdxl_image_to_image if requested_pipeline == 'image-to-image'
      return :sdxl_inpaint if requested_pipeline == 'inpaint'
      return :sdxl_text_to_image
    elsif model_info[:family] == 'latent-consistency'
      return :lcm_image_to_image if requested_pipeline == 'image-to-image'
      return :lcm_text_to_image
    else
      return requested_pipeline.gsub('-', '_').to_sym
    end
  end

  # Create an HTTP POST request with the given parameters
  def self.create_request(uri, prompt, model:, **options)
    request = Net::HTTP::Post.new(uri)
    request['Authorization'] = "Bearer #{@api_key}"
    request['accept'] = 'application/json'
    request['content-type'] = 'application/json'

    body = { prompt: prompt }.merge(options)
    body[:model] = model unless %w[essential essential-v2].include?(model)

    request.body = body.to_json
    request
  end

  # Make an HTTP POST request
  def self.make_post_request(uri, request)
    Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
      http.request(request)
    end
  end

  # Make an HTTP GET request
  def self.make_get_request(uri, request)
    Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
      http.request(request)
    end
  end

  # Handle the API response, raising errors if present
  def self.handle_response(response)
    case response
    when Net::HTTPSuccess
      result = JSON.parse(response.body)
      if result['error']
        raise StandardError, "Error: #{result['error']['message']} (code: #{result['error']['code']})"
      end
      result
    else
      error_message = "HTTP Error: #{response.message} (code: #{response.code})"
      verbose_message = JSON.parse(response.body)['error']['message'] rescue nil
      error_message += " - #{verbose_message}" if verbose_message
      raise StandardError, error_message
    end
  end

  # Validate if the input is a valid file path or base64 string
  def self.validate_file_or_base64(input)
    if File.exist?(input)
      Base64.strict_encode64(File.read(input))
    elsif input.match?(/\A([A-Za-z0-9+\/]{4})*([A-Za-z0-9+\/]{3}=|[A-Za-z0-9+\/]{2}==)?\z/)
      input
    else
      raise ArgumentError, 'Invalid file path or base64 string'
    end
  end

  # Parse the response from the models API to extract model details
  def self.parse_models_response(response)
    models = {}
    case response
    when Net::HTTPSuccess
      JSON.parse(response.body).each do |model|
        models[model['id']] = {
          name: model['name'],
          family: model['family'],
          pipelines: model['pipelines'],
          base_resolution: model['base_resolution'],
          price: model['price'],
          author_url: model['author_url'],
          license_url: model['license_url']
        }
      end
    else
      raise StandardError, "HTTP Error: #{response.message} (code: #{response.code})"
    end
    models
  end
end
