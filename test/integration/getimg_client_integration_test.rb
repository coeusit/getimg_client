require_relative "../test_helper"
require 'stringio'

class GetimgClientIntegrationTest < Minitest::Test
  def setup
    @base_image = Base64.strict_encode64(File.read(File.join(__dir__, "sample_image.jpeg")))
    @small_image = Base64.strict_encode64(File.read(File.join(__dir__, "small_sample_image.jpeg")))
    @mask_image = Base64.strict_encode64(File.read(File.join(__dir__, "mask_image.jpeg")))
  end

  def test_generate_text_to_image
    puts "-- Running test_generate_text_to_image..."
    result = GetimgClient.generate_image("A scenic landscape", model: :stable_diffusion_v1_5, width: 512, height: 512)
    assert result["image"]
    save_image(result["image"], "test_generate_text_to_image")
  end

  def test_generate_image_to_image
    puts "-- Running test_generate_image_to_image..."
    result = GetimgClient.generate_image("Enhance the scene", model: :stable_diffusion_v1_5, base_image: @base_image)
    assert result["image"]
    save_image(result["image"], "test_generate_image_to_image")
  end

  def test_generate_controlnet
    puts "-- Running test_generate_controlnet..."
    result = GetimgClient.generate_image("Outline of a landscape", model: :stable_diffusion_v1_5, base_image: @base_image, controlnet: "canny-1.1")
    assert result["image"]
    save_image(result["image"], "test_generate_controlnet")
  end

  def test_generate_instruct
    puts "-- Running test_generate_instruct..."
    result = GetimgClient.generate_image("Make it night time", model: :instruct_pix2pix, base_image: @base_image)
    assert result["image"]
    save_image(result["image"], "test_generate_instruct")
  end

  def test_generate_inpaint
    puts "-- Running test_generate_inpaint..."
    result = GetimgClient.generate_image("Fill the white area with sky", model: :stable_diffusion_v1_5_inpainting, base_image: @base_image, mask_image_path: @mask_image)
    assert result["image"]
    save_image(result["image"], "test_generate_inpaint")
  end

  def test_face_fix
    puts "-- Running test_face_fix..."
    result = GetimgClient.generate_image("Fix faces", model: :gfpgan_v1_3, base_image: @base_image)
    assert result["image"]
    save_image(result["image"], "test_face_fix")
  end

  def test_upscale
    puts "-- Running test_upscale..."
    result = GetimgClient.generate_image("Upscale image", model: :real_esrgan_4x, base_image: @small_image, scale: 4)
    assert result["image"]
    save_image(result["image"], "test_upscale")
  end

  def test_generate_text_to_image_essential
    puts "-- Running test_generate_text_to_image_essential..."
    result = GetimgClient.generate_image("A modern cityscape")
    assert result["image"]
    save_image(result["image"], "test_generate_text_to_image_essential")
  end

  def test_logging_to_rails_logger
    skip "Rails not defined, skipping Rails logger test" unless defined?(Rails) && (ENV['RAILS_ENV'] === 'development' || ENV['RAILS_ENV'].nil?)

    original_logger = Rails.logger
    log_output = StringIO.new
    Rails.logger = Logger.new(log_output)

    GetimgClient.generate_image("A scenic landscape", model: :stable_diffusion_v1_5, width: 512, height: 512)

    assert_includes log_output.string, "Generating image with the following details:"
    assert_includes log_output.string, "Prompt: A scenic landscape"

    Rails.logger = original_logger
  end

  def test_logging_to_puts
    ENV['GETIMG_LOG'] = '1'
    log_output = StringIO.new
    $stdout = log_output

    GetimgClient.generate_image("A scenic landscape", model: :stable_diffusion_v1_5, width: 512, height: 512)

    assert_includes log_output.string, "Generating image with the following details:"
    assert_includes log_output.string, "Prompt: A scenic landscape"

    $stdout = STDOUT
    ENV.delete('GETIMG_LOG')
  end

  private

  def save_image(base64_image, filename)
    File.open(File.join(__dir__, "../../tmp/#{filename}.jpeg"), "wb") do |file|
      file.write(Base64.decode64(base64_image.split(",").last))
    end
  end
end
