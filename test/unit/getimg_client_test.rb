require_relative "../test_helper"

class GetimgClientTest < Minitest::Test
  def test_retrieve_models
    models = GetimgClient.retrieve_models
    assert models.any?
  end

  def test_get_balance
    balance = GetimgClient.get_balance
    assert balance["amount"]
  end
end
